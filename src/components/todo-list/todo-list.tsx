import { Component, h,} from '@stencil/core';


@Component({
  tag: 'todo-list',
  styleUrl: 'todo-list.css',
  shadow: true,
})
export class TodoList {

  render() {
    return (
      <ul class="list">
      </ul>
    );
  }

}

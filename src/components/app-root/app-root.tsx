import { Component, Host, h } from '@stencil/core';
export interface TodoItem {
  id: number;
  text: string;
  completed: boolean;
}

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
  shadow: true,
})

export class AppRoot {
  todoList: TodoItem[] = [];

  addTodoItem(text: string) {
    const newTodoItem: TodoItem = {
      id: this.todoList.length + 1,
      text,
      completed: false,
    };

    this.todoList.push(newTodoItem);
  }

  toggleTodoItem(id: number) {
    this.todoList = this.todoList.map((todoItem) =>
      todoItem.id === id ? { ...todoItem, completed: !todoItem.completed } : todoItem
    );
  }

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }

}
